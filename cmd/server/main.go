package main

import (
	"basic/config"
	"basic/router"
)

func main() {
	e := router.Init()

	e.Logger.Fatal(e.Start(":" + config.ListenTo.Port))
}
