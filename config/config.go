package config

import (
	"github.com/joho/godotenv"
	"os"
)

const (
	PRODUCTION = "PRODUCTION"
)

type targetServer struct {
	Address string
	Port    string
}

var (
	ListenTo targetServer
	Environtment string
)

func init()  {
	var err error

	err = godotenv.Load()
	if err != nil {
		panic(err)
	}

	Environtment = os.Getenv("ENVIRONTMENT")
	ListenTo = targetServer{
		Address: os.Getenv("LISTEN_ADDRESS"),
		Port:    os.Getenv("LISTEN_PORT"),
	}
}

