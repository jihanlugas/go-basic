package router

import (
	"basic/config"
	"basic/controller"
	"fmt"
	"github.com/labstack/echo/v4"
)


type host struct {
	echo *echo.Echo
}

func Init() *echo.Echo {
	//var err error
	var listenPort string
	hosts := make(map[string]*host)
	//if listenPort, err = 1323; err != nil {
	//	panic(err)
	//}

	listenPort = config.ListenTo.Port

	web := websiteRouter()
	webDomain := "localhost"
	hosts[webDomain] = &host{web}
	hosts[fmt.Sprintf("%s:%s", webDomain, listenPort)] = hosts[webDomain]


	web.GET("/", controller.Ping)


	e := echo.New()
	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		hst := hosts[req.Host]

		if hst == nil {
			err = echo.ErrNotFound
		} else {
			hst.echo.ServeHTTP(res, req)
		}

		return
	})

	return e
}