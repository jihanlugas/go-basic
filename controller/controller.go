package controller

import (
	"basic/response"
	"github.com/labstack/echo/v4"
)


func Ping(c echo.Context) error {
	return response.StatusOk("ようこそ、美しいの世界へ", response.Payload{}).SendJSON(c)
}